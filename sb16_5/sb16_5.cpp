#include <iostream>
#include <time.h>

int main()
{
    // 1. В главном исполняемом файле (файл, в котором находится функция main) создайте двумерный массив 
    // размерности N × N и заполните его так, чтобы элемент с индексами i и j был равен i + j. 
    const int N = 5;
    int array[N][N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
        }
    }

    // 2. Выведите этот массив в консоль. 
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << array[i][j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    // Выведите сумму элементов в строке массива, индекс которой равен остатку деления текущего числа 
    // календаря на N (в двумерном массиве a[i][j], i — индекс строки).
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int sum = 0, stroke = buf.tm_mday % N;
    for (int i = 0; i < N; i++)
    {
        sum += array[i][stroke];
    }
    std::cout << sum;
}